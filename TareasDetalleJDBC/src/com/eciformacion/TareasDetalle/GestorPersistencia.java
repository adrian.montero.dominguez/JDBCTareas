package com.eciformacion.TareasDetalle;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

public class GestorPersistencia {
	private Connection conn;
	
	// constructor
	public GestorPersistencia(Connection conn) {
		super();
		this.conn=conn;
	}

	// metodos a�adir
	public void anadirTarea(Tareas t) throws SQLException {
		PreparedStatement ps = conn.prepareStatement("INSERT INTO tareas (nombre) VALUES(?)");
		ps.setString(1, t.getNombre());
		ps.executeUpdate();

	}

	public void anadirDetalle(Detalle d) throws SQLException {
		PreparedStatement ps = conn.prepareStatement("INSERT INTO detalle (idTarea,nombre,realizado) VALUES(?,?,?)");
		ps.setInt(1, d.getIdTarea());
		ps.setString(2, d.getNombre());
		ps.setBoolean(3, d.isRealizado());
		ps.executeUpdate();
	}

	// metodos eliminar
	public void eliminarTarea(String nombre) throws SQLException {
		PreparedStatement ps = conn
				.prepareStatement("DELETE t,d FROM detalle d INNER JOIN tareas t ON t.id=d.idTarea WHERE t.nombre=?");
		ps.setString(1, nombre);
		ps.executeUpdate();

	}

	public void eliminarDetalle(String nombre) throws SQLException {
		PreparedStatement ps = conn.prepareStatement("DELETE FROM detalle WHERE nombre='" + nombre + "'");
		ps.executeUpdate();
	}

	// modificacion
	public void modificarTarea(String nombre, String nuevonombre) throws SQLException {
		PreparedStatement ps = conn.prepareStatement("UPDATE tareas SET nombre=? WHERE nombre='" + nombre + "'");
		ps.setString(1, nuevonombre);
		ps.executeUpdate();

	}

	public void modificarNombreDetalle(String nombre, String nuevonombre) throws SQLException {
		PreparedStatement ps = conn.prepareStatement("UPDATE detalle SET nombre=? WHERE nombre='" + nombre + "'");
		ps.setString(1, nuevonombre);
		ps.executeUpdate();

	}

	public void modificarDetalleRealizado(String nombre, boolean bl) throws SQLException {
		PreparedStatement ps = conn.prepareStatement("UPDATE detalle SET realizada=? WHERE nombre='" + nombre + "'");
		ps.setBoolean(1, bl);
		ps.executeUpdate();

	}

	// mostrar
	public void mostrarTarea() throws SQLException {
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM tareas");
		while (rs.next()) {
			System.out.println("id: " + rs.getInt(1) + " Tarea: " + rs.getString(2));
		}

	}

	public void mostrarDetalle() throws SQLException {
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM detalle");
		while (rs.next()) {
			System.out.println("id: " + rs.getInt(1) +"idDetalle: " + rs.getInt(2)+ " Nombre: " + rs.getString(3)+" Realizado: "+rs.getBoolean(4));
		}

	}
}
