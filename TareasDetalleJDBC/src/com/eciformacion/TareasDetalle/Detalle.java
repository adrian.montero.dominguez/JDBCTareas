package com.eciformacion.TareasDetalle;

public class Detalle {

	private int idTarea;
	private String nombre;
	boolean realizado;
	
	//construct
	public Detalle(int idTarea, String nombre, boolean realizado) {
		super();
		this.idTarea = idTarea;
		this.nombre = nombre;
		this.realizado = realizado;
	}
	
	//getter Setter
	public int getIdTarea() {
		return idTarea;
	}
	public void setIdTarea(int idTarea) {
		this.idTarea = idTarea;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public boolean isRealizado() {
		return realizado;
	}
	public void setRealizado(boolean realizado) {
		this.realizado = realizado;
	}
	
	
}
