package com.eciformacion.TareasDetalle;

import java.sql.*;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub

		int opcion, iddetalle, idtarea, realizada;
		String nombre, nuevonombre;
		boolean bl = false;
		Connection conn = null;
		
		
		Tareas tarea;
		Detalle detalle;

		Scanner scanner = new Scanner(System.in);
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException ex) {
			System.out.println("No existe el driver de mysql.");
		}
		//https://gitlab.com/adrian.montero.dominguez/JDBCTareas.git
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/prueba1", "root", "");
			GestorPersistencia gestor = new GestorPersistencia((com.mysql.jdbc.Connection) conn);
			do {
				System.out.println("1.A�adir tarea.");
				System.out.println("2.A�adir detalle.");
				System.out.println("3.Eliminar tarea.");
				System.out.println("4.Eliminar detalle.");
				System.out.println("5.Modificar tarea.");
				System.out.println("6.Modificar nombre detalle.");
				System.out.println("7.Modificar realizacion del detalle.");
				System.out.println("8.Mostrar Tareas y detalles.");
				System.out.println("9.Salir.");

				opcion = scanner.nextInt();
				switch (opcion) {
				case 1:
					scanner.nextLine();
					System.out.println("Introduzca la nueva tarea: ");
					nombre = scanner.nextLine();
					tarea = new Tareas(nombre);
					gestor.anadirTarea(tarea);
					System.out.println("a�adido");
					System.out.println("////////////////////");
					gestor.mostrarTarea();
					System.out.println("////////////////////");
					break;
				case 2:
					scanner.nextLine();
					System.out.println("Introduzca el nuevo detalle: ");
					nombre = scanner.nextLine();
					System.out.println("id de la tarea a la que corresponde: ");
					idtarea = scanner.nextInt();
					do {
						System.out.println("�Esta la tarea realizada? para confirmar 1 sino 0.");
						realizada = scanner.nextInt();
						if (realizada == 1) {
							bl = true;
						} else if (realizada == 0) {
							bl = false;
						} else {
							System.out.println("Opcion no contemplada");
						}
					} while (realizada != 1 && realizada != 0);
					detalle = new Detalle(idtarea, nombre, bl);
					gestor.anadirDetalle(detalle);
							detalle.isRealizado();
					System.out.println("a�adido");
					System.out.println("////////////////////");
					gestor.mostrarDetalle();
					System.out.println("////////////////////");
					break;
				case 3:
					scanner.nextLine();
					System.out.println("Nombre de la tarea que desea Eliminar.");
					System.out.println("Se eliminaran sus detalles tambien!!.");
					nombre = scanner.nextLine();
					
					gestor.eliminarTarea(nombre);
					System.out.println("///////////////////////////////");
					gestor.mostrarTarea();
					System.out.println("******************************");
					gestor.mostrarDetalle();
					System.out.println("///////////////////////////////");
					break;
				case 4:
					scanner.nextLine();
					System.out.println("Nombre el detalle que desea Eliminar.");
					nombre = scanner.nextLine();
					gestor.eliminarDetalle(nombre);
					System.out.println("///////////////////////////////");
					gestor.mostrarDetalle();
					System.out.println("///////////////////////////////");
					break;
				case 5:
					scanner.nextLine();
					System.out.println("Seleccione la tarea a modificar:(introduzca nombre)");
					nombre = scanner.nextLine();
					System.out.println("Nuevo nombre: ");
					nuevonombre = scanner.nextLine();
					gestor.modificarTarea(nombre, nuevonombre);
					break;
				case 6:
					scanner.nextLine();
					System.out.println("Seleccione el detalle a modificar:(introduzca nombre)");
					nombre = scanner.nextLine();
					System.out.println("Nuevo nombre: ");
					nuevonombre = scanner.nextLine();
					gestor.modificarNombreDetalle(nombre, nuevonombre);
					System.out.println("///////////////////////////////");
					gestor.mostrarDetalle();
					System.out.println("///////////////////////////////");
					break;
				case 7:
					scanner.nextLine();
					System.out.println("Seleccione el detalle a modificar:");
					nombre = scanner.nextLine();
					System.out.println("Seleccione si esta realizada o no(1 o 0): ");
					do {
						realizada = scanner.nextInt();
						if (realizada == 1) {
							bl = true;
						} else if (realizada == 0) {
							bl = false;
						} else {
							System.out.println("Opcion no contemplada");
						}
					} while (realizada != 1 && realizada != 0);
					gestor.modificarDetalleRealizado(nombre, bl);
					System.out.println("///////////////////////////////");
					gestor.mostrarDetalle();
					System.out.println("///////////////////////////////");
					break;
				case 8:
					System.out.println("///////////////////////////////");
					gestor.mostrarTarea();
					System.out.println("******************************");
					gestor.mostrarDetalle();
					System.out.println("///////////////////////////////");
					break;
				case 9:
					System.out.println("Saliendo...");
					break;
				default:
					System.out.println("Opcion no contemplada.");
					break;
				}
			} while (opcion != 9);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			conn.close();
		}
	}

}
