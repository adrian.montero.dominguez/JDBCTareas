package com.eciformacion.TareasDetalle;

public class Tareas {

	private String nombre;

	// construct
	public Tareas(String nombre) {
		super();
		this.nombre = nombre;
	}

	// getter setter
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
